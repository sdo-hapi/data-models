<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="1.0">

    <xsl:output method="text" encoding="UTF-8" indent="no"/>
    <xsl:strip-space elements="*"/>

    <xsl:param name="delimiter" select="','"/>
    <xsl:param name="quote" select="'&quot;'"/>
    <xsl:param name="break" select="'&#xA;'"/>


    <!-- Load configuration mappings -->
    <xsl:variable name="mappings" select="document('documentation.xml')/appinfo-annotation-rewriter/entry"/>
    <xsl:variable name="cenMappings" select="document('cen-specifics.xml')/appinfo-annotation-rewriter/entry"/>
    <xsl:variable name="cenelecMappings" select="document('cenelec-specifics.xml')/appinfo-annotation-rewriter/entry"/>
    <xsl:variable name="iecMappings" select="document('iec-specifics.xml')/appinfo-annotation-rewriter/entry"/>
    <xsl:variable name="isoMappings" select="document('iso-specifics.xml')/appinfo-annotation-rewriter/entry"/>

    <!-- Process nodes (attributes and others further down) -->
    <xsl:template match="/">
        <xsl:value-of select="'&#xFEFF;'"/> <!-- Emit UTF-8 BOM so that Excel will open in UTF-8 -->
        <!-- Column headers -->
        <xsl:text>Key,Description</xsl:text>
        <xsl:value-of select="$break"/>


        <!-- NEW FILE -->
        <xsl:text>NEW_FILE,documentation.xml</xsl:text>
        <xsl:value-of select="$break"/>

        <xsl:for-each select="$mappings">
            <xsl:call-template name="csvLine"/>
        </xsl:for-each>



        <!-- NEW FILE -->
        <xsl:text>NEW_FILE,iso-specifics.xml</xsl:text>
        <xsl:value-of select="$break"/>

        <xsl:for-each select="$isoMappings">
            <xsl:call-template name="csvLine"/>
        </xsl:for-each>



        <!-- NEW FILE -->
        <xsl:text>NEW_FILE,iec-specifics.xml</xsl:text>
        <xsl:value-of select="$break"/>

        <xsl:for-each select="$iecMappings">
            <xsl:call-template name="csvLine"/>
        </xsl:for-each>


        <!-- NEW FILE -->
        <xsl:text>NEW_FILE,cen-specifics.xml</xsl:text>
        <xsl:value-of select="$break"/>

        <xsl:for-each select="$cenMappings">
            <xsl:call-template name="csvLine"/>
        </xsl:for-each>


        <!-- NEW FILE -->
        <xsl:text>NEW_FILE,cenelec-specifics.xml</xsl:text>
        <xsl:value-of select="$break"/>

        <xsl:for-each select="$cenelecMappings">
            <xsl:call-template name="csvLine"/>
        </xsl:for-each>

    </xsl:template>



    <xsl:template name="csvLine">
        <xsl:call-template name="escapeText">
            <xsl:with-param name="value" select="Key"/>
        </xsl:call-template>

        <xsl:value-of select="$delimiter"/>

        <xsl:call-template name="escapeText">
            <xsl:with-param name="value" select="Description"/>
        </xsl:call-template>
        <xsl:value-of select="$break"/>
    </xsl:template>



    <xsl:template name="escapeText">
        <xsl:param name="value"/>
        <xsl:if test="$value">
            <xsl:value-of select="$quote"/>
            <xsl:value-of select="$value"/>
            <xsl:value-of select="$quote"/>
        </xsl:if>
        <!-- Quote the value if required - This will NOT work with XSLT 1.0 due to replace() missing
        <xsl:choose>
          <xsl:when test="contains($value, '&quot;')">
            <xsl:variable name="x" select="replace($value, '&quot;',  '&quot;&quot;')"/>
            <xsl:value-of select="concat('&quot;', $x, '&quot;')"/>
          </xsl:when>
          <xsl:when test="contains($value, $delimiter)">
            <xsl:value-of select="concat('&quot;', $value, '&quot;')"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$value"/>
          </xsl:otherwise>
        </xsl:choose>
     -->
    </xsl:template>

</xsl:stylesheet>