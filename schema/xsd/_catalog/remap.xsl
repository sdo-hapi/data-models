<?xml version="1.0" encoding="UTF-8"?>
<!--
    Utility to enrich a XSD schema with cross-references to a Data Catalog, creating the xs:documentation tags from what is found in the catalog.
    This transformation reads in the 5 components of the data catalog hosted in the same directory, see the xsl:variable below.
    Then it parses the input XSD and produces an output XSD of similar contents, except that all xs:documentation elements for which
    a xs:appinfo has been defined in the catalog will be replaced by their value.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="1.0">

    <xsl:output indent="yes"/>
    <xsl:strip-space elements="*"/>

    <!-- Load configuration mappings -->
    <xsl:variable name="mappings" select="document('documentation.xml')/appinfo-annotation-rewriter/entry"/>
    <xsl:variable name="cenMappings" select="document('cen-specifics.xml')/appinfo-annotation-rewriter/entry"/>
    <xsl:variable name="cenelecMappings" select="document('cenelec-specifics.xml')/appinfo-annotation-rewriter/entry"/>
    <xsl:variable name="iecMappings" select="document('iec-specifics.xml')/appinfo-annotation-rewriter/entry"/>
    <xsl:variable name="isoMappings" select="document('iso-specifics.xml')/appinfo-annotation-rewriter/entry"/>

    <!-- Process nodes (attributes and others further down) -->
    <xsl:template match="*">
        <xsl:choose>
            <!-- Will trig on all xs:annotation nodes that do have a child xs:appinfo node -->
            <xsl:when test="xs:appinfo">
                <xsl:variable name="AppInfoToken" select="xs:appinfo"/>
                <xsl:variable name="Contents" select="$mappings[Key = $AppInfoToken]/Description"/>
                <xsl:element name="xs:annotation">
                    <xsl:element name="xs:appinfo"><xsl:attribute name="source">https://bitbucket.org/sdo-hapi/data-models</xsl:attribute><xsl:value-of select="$AppInfoToken"/></xsl:element>
                    <xsl:choose>
                        <xsl:when test="$Contents">
                            <xsl:element name="xs:documentation">
                                <xsl:if test="normalize-space($Contents)">
                                    <xsl:text>&#xa;</xsl:text>
                                    <xsl:value-of select="normalize-space($Contents)"/>
                                </xsl:if>
                                <!-- CEN specific contents -->
                                <xsl:variable name="cenContents" select="$cenMappings[Key = $AppInfoToken]/Description"/>
                                <xsl:if test="$cenContents">
                                    <xsl:text>&#xa;@CEN: </xsl:text>
                                    <xsl:value-of select="normalize-space($cenContents)"/>
                                </xsl:if>
                                <!-- CENELEC specific contents -->
                                <xsl:variable name="cenelecContents" select="$cenelecMappings[Key = $AppInfoToken]/Description"/>
                                <xsl:if test="$cenelecContents">
                                    <xsl:text>&#xa;@CENELEC: </xsl:text>
                                    <xsl:value-of select="normalize-space($cenelecContents)"/>
                                </xsl:if>
                                <!-- ISO specific contents -->
                                <xsl:variable name="iecContents" select="$iecMappings[Key = $AppInfoToken]/Description"/>
                                <xsl:if test="$iecContents">
                                    <xsl:text>&#xa;@IEC: </xsl:text>
                                    <xsl:value-of select="normalize-space($iecContents)"/>
                                </xsl:if>
                                <!-- ISO specific contents -->
                                <xsl:variable name="isoContents" select="$isoMappings[Key = $AppInfoToken]/Description"/>
                                <xsl:if test="$isoContents">
                                    <xsl:text>&#xa;@ISO: </xsl:text>
                                    <xsl:value-of select="normalize-space($isoContents)"/>
                                </xsl:if>
                            </xsl:element>
                        </xsl:when>
                        <xsl:otherwise>
                            <!-- Make XSLT fail but I don't see the effective error message so rather unusable. So set terminate="no" and rerun, look for "ERROR" in the output -->
                            <xsl:message terminate="yes">ERROR: No mapping defined for appinfo key: <xsl:value-of select="$AppInfoToken"/></xsl:message>
                            <!-- Report an error in the output document -->
                            <xsl:element name="xs:documentation">ERROR: No mapping defined for appinfo key: <xsl:value-of select="$AppInfoToken"/></xsl:element>
                        </xsl:otherwise>
                    </xsl:choose>

                </xsl:element>
            </xsl:when>

            <!-- Otherwise copy as is -->
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@* | node()"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <!-- Attributes and comments copied without transformation -->
    <xsl:template match="comment() | @*">
        <xsl:copy/>
    </xsl:template>

</xsl:stylesheet>