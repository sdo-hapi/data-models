<?xml version="1.0" encoding="UTF-8"?>
<!--
    This XSLT is a non-functional report that indicates which documentation tags have not yet been cross-referenced in the catalog.
    This extracts
    1) all xs:annotation XML elements that have a child xs:documentation
    2) on elements that are not xs:enumeration - because not all enums need to be documented!
    3) that lack the xs:appinfo attribute.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="1.0">

    <xsl:output indent="yes"/>
    <xsl:strip-space elements="*"/>


    <!-- not-yet ready -->
    <xsl:template match="/">
    <xsl:for-each select="//*[name() != 'xs:enumeration']/xs:annotation[xs:documentation and not(xs:appinfo)]">
        <!-- <xsl:copy-of select="node() | @*"/> -->
        <xsl:value-of select="../@name" />
        <xsl:text>&#xa;</xsl:text>

    </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
