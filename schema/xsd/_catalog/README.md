#### Data Catalog

This directory intends to separate the documnentation of our XSDs from the actual data model they describe.

The purpose is to ease maintenance and integrate with corporate Data Catalogs.
In this directory there are only semantic definitions of our data elements.

In the XSDs, we add an xs:appinfo to all xs:annotation elements, such as below:

```
<xs:annotation>
   <xs:appinfo source="https://bitbucket.org/sdo-hapi/data-models">hapi:docu:030</xs:appinfo>
   <xs:documentation>
      Some documentation that will be found in the catalog, and copied here automatically thanks to
      the cross-reference in the xs:appinfo element
   </xs:documentation>
</xs:annotation>
```

The "remap.xsl" stylesheet will process one XSD by identifying all xs:appinfo elements and replace the
following xs:documnentation tag automatically.

If you use Java, the following command (or any tooling from your IDE) will transform one of the XSDs to
embed the documentation:

```
java -Dxslt.file=data-models/schema/xsd/_catalog/remap.xsl -Dxslt.input=data-models/schema/xsd/common/harmonized-commons.xsd -Dxslt.output=data-models/schema/xsd/common/harmonized-commons-out.xsd -Dfile.encoding=UTF-8 org.intellij.plugins.xslt.run.rt.XSLTRunner
```

In case the XSLT transformation fails (truncated output file), follow these instructions:

1. Open "remap.xsl"
2. Locate "<xsl:message terminate"
3. Switch the terminate flag to "no"
4. Rerun the XSLT transformation
5. Look for "ERROR" in the output, it will indicate which property was absent from the catalog
6. Fix, and rerun

