<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:vc="http://www.w3.org/2007/XMLSchema-versioning" elementFormDefault="qualified"
    vc:minVersion="1.0" vc:maxVersion="1.1">
    <xs:import namespace="http://www.w3.org/XML/1998/namespace" schemaLocation="http://www.w3.org/2001/xml.xsd"/>
    <xs:include schemaLocation="../common/harmonized-commons.xsd" id="harmonized-commons"/>
    <xs:annotation>
        <xs:documentation>Schema definition for publications, it is inspired from the FRBR model</xs:documentation>
    </xs:annotation>
    <!--

      TYPES: Note: For elements named "xxxType", the name will be named "xxxTypeType", that's a classic, not an error!

      -->
    <xs:simpleType name="publicationTypeType">
        <xs:annotation>
            <xs:appinfo source="https://bitbucket.org/sdo-hapi/data-models">hapi:docu:044</xs:appinfo>
            <xs:documentation>
                The type of a publication; describes what the work process to produce intellectual content aims at. Producing content of that type is
                typically supported by a project, possibly involving consensus and voting. Types of releaseItems, corresponding to the various
                representations of one publication, are defined in "releaseItemTypeType".
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="STANDARD"/>
            <xs:enumeration value="PUBLICATION"/>
            <xs:enumeration value="COLLECTION"/>
            <xs:enumeration value="CORRIGENDUM">
                <xs:annotation>
                    <xs:documentation>
                        @ISO: No longer produced, but we have legacy corrigenda. A separate publication in itself because corrigenda used to go to voting.
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="REDLINE">
                <xs:annotation>
                    <xs:documentation>
                        @IEC: the redline is a separate publication with a different abstract, disclaimer, and price.
                        @ISO: the REDLINE is an attribute of the releaseItem, not the parent publication. Therefore, it is defined on
                        "releaseItemTypeType". The publication type will be "STANDARD".
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="PRERELEASE"/>
            <xs:enumeration value="INTERPRETATION_SHEET"/>
            <xs:enumeration value="CONSOLIDATED"/>
            <xs:enumeration value="COMMENTED"/>
            <xs:enumeration value="EXTENDED"/>
            <xs:enumeration value="SERIES"/>
            <xs:enumeration value="PACK"/>
            <xs:enumeration value="OTHER">
                <xs:annotation>
                    <xs:documentation>
                        @CENELEC: the OTHER type will be used for BALLOTREPORTs until the working documents API will be in production. 
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>

        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="priceCodeType">
        <xs:annotation>
            <xs:appinfo source="https://bitbucket.org/sdo-hapi/data-models">hapi:docu:045</xs:appinfo>
            <xs:documentation>
                The SDO-specific price code prefixed with the SDO name. Example(s): "iec:A" or "iso:XY".
                @ISO: The data element is preferrably named "Price Group", it used to be called "Price Code" in former ISONET.
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:token">
            <xs:pattern value="iec:[\w:_/+-]+"/>
            <xs:pattern value="iso:[\w:_/+-]+"/>
            <!-- There are no price codes for CEN/CENELEC publications -->
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="publicationStatusType">
        <xs:annotation>
            <xs:appinfo source="https://bitbucket.org/sdo-hapi/data-models">hapi:docu:046</xs:appinfo>
            <xs:documentation>
                Status of life-cycle of a publication's content (for publications related to standards, is related to the current projetct's status).
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:string">
            <xs:enumeration value="IN_DEVELOPMENT"/>
            <!-- TODO(ISO): see if anyone uses this -->
            <xs:enumeration value="DRAFT"/>
            <xs:enumeration value="PUBLISHED"/>
            <xs:enumeration value="REPLACED"/>
            <xs:enumeration value="REVISED"/>
            <xs:enumeration value="WITHDRAWN"/>
            <!-- TODO(ISO): ISO 9001:2000 is REVISED by a more recent, but also in status WITHDRAWN -->
        </xs:restriction>
    </xs:simpleType>
    <!--

      ELEMENTS: Names of XML elements that may appear in various context and that always have the same definition.

      -->
    <xs:element name="stabilityDate" type="xs:date">
        <xs:annotation>
            <xs:appinfo source="https://bitbucket.org/sdo-hapi/data-models">hapi:docu:047</xs:appinfo>
            <xs:documentation>
                @IEC: Planned date for the next review.
                @ISO: Not used.
            </xs:documentation>
        </xs:annotation>
    </xs:element>
    <xs:element name="publicationsRoot">
        <xs:annotation>
            <xs:appinfo source="https://bitbucket.org/sdo-hapi/data-models">hapi:docu:048</xs:appinfo>
            <xs:documentation>
                The outermost element of the publications feed, when one or several publications are returned. This only provides contextual
                information, plus a set of individual publication.
            </xs:documentation>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element ref="source"/>
                <xs:element ref="publication" minOccurs="0" maxOccurs="unbounded"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="publication">
        <xs:annotation>
            <xs:appinfo source="https://bitbucket.org/sdo-hapi/data-models">hapi:docu:049</xs:appinfo>
            <xs:documentation>
                The "publication" corresponds to the "Expression" level of the FRBR model:
                https://en.wikipedia.org/wiki/Functional_Requirements_for_Bibliographic_Records It is an abstract unity of intellectual contents that
                has been produced at one moment in time by a work process. A standardization project produces several "publications" at its various
                stage of development. One "publication" materializes into several "releaseItems", each being different renditions of the same
                intellectual content in various formats or languages. See also the documents in this GIT repository under design/publications.
            </xs:documentation>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:group ref="requiredUrnIdentification"/>
                <xs:element name="publicationType" type="publicationTypeType"/>
                <!-- Avoid xs:choice, it breaks XJC generator. Just inline the 3 values containaed in the 2 choices -->
                <xs:element ref="reference" minOccurs="0"/>
                <xs:element ref="sortKey" minOccurs="0"/>
                <xs:group ref="stdIdentifier" minOccurs="0"/>
                <!-- Already in "stdIdentifer" group:  <xs:element name="edition" type="nonEmptyString" minOccurs="0"/> -->
                <xs:element ref="title" maxOccurs="unbounded"/>
                <xs:element ref="committee" minOccurs="0"/>
                <xs:element ref="abstract" minOccurs="0" maxOccurs="unbounded"/>
                <xs:element ref="classifications" minOccurs="0">
                    <xs:annotation>
                        <xs:documentation>
                            @IEC: There are publications without project; particularly ISO/IEC standards
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element ref="status"/>
                <xs:element name="publicationStage" type="xs:token" minOccurs="0">
                    <xs:annotation>
                        <xs:appinfo source="https://bitbucket.org/sdo-hapi/data-models">hapi:docu:129</xs:appinfo>
                        <xs:documentation>
                            If this publication is related to a Project (is produced by a standard's development process), this defines the major
                            development stage which produced the publication's contents. The value is specific to the source organization (CEN,
                            CENELEC, IEC, ISO).
                            @CEN: The iteration may appear as in "ENQ", "ENQ2", etc.
                            @ISO: uses exclusively "CD", "DIS", "FDIS" and "IS"; they matches the rows 30, 40, 50, and 60 of the harmonized stage
                            codes matrix. In case the project's stages are re-iterated, only the last contents is exposed as a publication. Notice
                            that this value also appears in the publication urn (see samples).
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element ref="isbn" minOccurs="0"/>
                <xs:element ref="publicationDate" minOccurs="0">
                    <xs:annotation>
                        <xs:appinfo source="https://bitbucket.org/sdo-hapi/data-models">hapi:docu:051</xs:appinfo>
                        <xs:documentation>
                            @CEN: Date of availability (dav) date when the definitive text in the official language versions of an approved
                            CEN/CENELEC publication is distributed by the CEN-CENELEC Management Centre.
                            @CENELEC: Date of availability (dav) date when the definitive text in the official language versions of an approved
                            CEN/CENELEC publication is distributed by the CEN-CENELEC Management Centre.
                            @ISO: For the corresponding project, the "startDate" of the last stage 60.60. This means the last time a day 60.60 =
                            Publication was reached (normally there is only one).
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element ref="confirmationDate" minOccurs="0"/>
                <xs:element ref="stabilityDate" minOccurs="0"/>
                <xs:sequence minOccurs="0">
                    <xs:element name="dateOfAvailability" type="xs:date" minOccurs="0">
                        <xs:annotation>
                            <xs:appinfo source="https://bitbucket.org/sdo-hapi/data-models">hapi:docu:052</xs:appinfo>
                            <xs:documentation>
                                @CEN: Not used anymore.
                                @CENELEC: Not used anymore.
                                @ISO: Not used.
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="dateOfAnnouncement" type="xs:date" minOccurs="0">
                        <xs:annotation>
                            <xs:appinfo source="https://bitbucket.org/sdo-hapi/data-models">hapi:docu:214</xs:appinfo>
                            <xs:documentation>
                                @CEN: Date of announcement (doa) latest date by which the existence of an EN (and HD for CENELEC), a TS or a CWA has
                                to be announced at national level.
                                @CENELEC: Date of announcement (doa) latest date by which the existence of an EN (and HD for CENELEC), a TS or a CWA
                                has to be announced at national level.
                                @ISO: Not used.
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="dateOfPublication" type="xs:date" minOccurs="0">
                        <xs:annotation>
                            <xs:appinfo source="https://bitbucket.org/sdo-hapi/data-models">hapi:docu:053</xs:appinfo>
                            <xs:documentation>
                                @CEN: Date of publication (dop): latest date by which an EN has to be implemented at national level by publication of
                                an identical national standard or by endorsement as national standard 2.21
                                @ISO: Not used.
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                    <xs:element name="dateOfWithdrawal" type="xs:date" minOccurs="0">
                        <xs:annotation>
                            <xs:appinfo source="https://bitbucket.org/sdo-hapi/data-models">hapi:docu:054</xs:appinfo>
                            <xs:documentation>
                                Date of withdrawal (dow) latest date by which national standards conflicting with an EN (and HD for CENELEC) have to
                                be withdrawn.
                                @ISO: Not used.
                            </xs:documentation>
                        </xs:annotation>
                    </xs:element>
                </xs:sequence>
                <xs:element ref="priceInfo" minOccurs="0">
                    <xs:annotation>
                        <xs:appinfo source="https://bitbucket.org/sdo-hapi/data-models">hapi:docu:056</xs:appinfo>
                        <xs:documentation>
                            Pricing information.
                            @CEN: not used
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
                <!--
                        Tentatively remove project's classifications off the publications feed.
                        Relations and classifications are anyway found on the projects feed.
                        <xs:element ref="classifications" minOccurs="0"/>
                        -->
                <xs:element ref="releaseItems" minOccurs="0"/>
                <xs:element name="project" minOccurs="0">
                    <xs:annotation>
                        <xs:appinfo source="https://bitbucket.org/sdo-hapi/data-models">hapi:docu:055</xs:appinfo>
                        <xs:documentation>
                            Optional reference from this publication to the project that created it.
                        </xs:documentation>
                    </xs:annotation>
                    <xs:complexType>
                        <xs:group ref="requiredUrnIdentification"/>
                    </xs:complexType>
                </xs:element>
                <xs:element ref="lastChangeTimestamp"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="content" type="nonEmptyString"/>
    <xs:element name="committee">
        <xs:complexType>
            <xs:sequence>
                <xs:group ref="requiredUrnIdentification"/>
                <xs:element ref="reference"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
    <xs:element name="status" type="publicationStatusType">
        <xs:annotation>
            <xs:appinfo source="https://bitbucket.org/sdo-hapi/data-models">hapi:docu:050</xs:appinfo>
            <xs:documentation>
                For publications associated to projects / standards, this will reflect the current project's status. The purpose of this data element
                is to make sure that publications returned by the API are still already, or still valid for use. See issue #4.
            </xs:documentation>
        </xs:annotation>
    </xs:element>
    <xs:element name="priceInfo">
        <xs:complexType>
            <xs:sequence>
                <xs:element name="priceCode" type="priceCodeType"/>
                <xs:element name="basePrice">
                    <xs:complexType>
                        <xs:sequence>
                            <xs:element name="amount" type="xs:decimal"/>
                            <xs:element name="currency" type="xs:token"/>
                        </xs:sequence>
                    </xs:complexType>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
</xs:schema>
