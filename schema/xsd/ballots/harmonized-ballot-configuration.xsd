<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:vc="http://www.w3.org/2007/XMLSchema-versioning"
    elementFormDefault="qualified"
    vc:minVersion="1.0" vc:maxVersion="1.1">
    <xs:import namespace="http://www.w3.org/XML/1998/namespace" schemaLocation="http://www.w3.org/2001/xml.xsd"/>
    <xs:include schemaLocation="../common/harmonized-commons.xsd" id="harmonized-commons"/>

    <xs:annotation>
        <xs:documentation>Schema definition for ballots</xs:documentation>
    </xs:annotation>

    <xs:element name="ballotConfigurationsRoot">
        <xs:complexType>
            <xs:sequence>
                <xs:element ref="source"/>
                <xs:element ref="ballotConfiguration" minOccurs="0" maxOccurs="unbounded"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>

    <xs:element name="ballotConfiguration">
        <xs:annotation>
            <xs:documentation>
                A ballotConfiguration describes common features of many ballots sharing, for example, the same
                questionnaire. The value of ballotConfiguration/id will be matching that of ballot/configurationId.
            </xs:documentation>
        </xs:annotation>
        <xs:complexType>
            <xs:sequence>
                <xs:element name="id">
                    <xs:annotation>
                        <xs:documentation>
                            This configuration id is referenced from the field ballot/ballotConfigurationId in the harmonized-ballots schema.
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="type">
                    <xs:annotation>
                        <xs:documentation>
                            The ballot type.
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="version">
                    <xs:annotation>
                        <xs:documentation>
                            A character string denoting the version for the above type of ballot. This may be a year, or a sequence number, or anything
                            that will make the pair (type, version) unique.
                            This value must be defined, however for legacy reasons there may be a placehoder, for example @ISO we use "unversioned"
                            when a give ballot template was not versioned, or is not yet versioned.
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="effectiveDate" type="xs:date" minOccurs="0">
                    <xs:annotation>
                        <xs:documentation>
                            When available, the effective date when a certain configuration for ballots became available.
                            Mostly for the case of the new version of a questionnaire.
                            This field is optional.
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="hasFormalResult" type="xs:boolean">
                    <xs:annotation>
                        <xs:documentation>
                            Indicate if those ballots end up with a formal result or if they only produce a report of votes / satistics.
                        </xs:documentation>
                    </xs:annotation>
                </xs:element>
                <xs:element name="logicRules" minOccurs="0">
                    <xs:complexType>
                        <xs:sequence>
                            <xs:element name="majorityRule" type="xs:string" minOccurs="0"/>
                            <!-- Only if we decide to use this
                            <xs:element name="againstRule" minOccurs="0"/>
                            <xs:element name="npParticipationRule" minOccurs="0"/>
                            -->
                        </xs:sequence>                       
                    </xs:complexType>
                </xs:element>
                <xs:element name="voteConfiguration" type="voteConfigurationType" minOccurs="0"/>
                <xs:element name="commentConfiguration" type="voteConfigurationType" minOccurs="0"/>
            </xs:sequence>
        </xs:complexType>
    </xs:element>

    <xs:complexType name="voteConfigurationType">
        <xs:annotation>
            <xs:documentation>
                Vote configuration.
            </xs:documentation>
        </xs:annotation>

        <xs:sequence>
            <xs:element name="numberOfQuestions"/>
            <xs:element name="questions">
                <xs:complexType>
                    <xs:sequence maxOccurs="unbounded">
                        <xs:element name="question" type="questionType"/>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
        </xs:sequence>
    </xs:complexType>

    <!-- <xs:complexType name="commentConfigurationType">
         <xs:annotation>
             <xs:documentation>
                 Comment configuration.
             </xs:documentation>
         </xs:annotation>
         <xs:sequence>
         </xs:sequence>
     </xs:complexType>-->

    <xs:complexType name="questionType">
        <xs:sequence>
            <xs:element name="id" minOccurs="0"/>
            <xs:element name="position" type="xs:decimal"/>
            <xs:element name="text"/>
            <xs:choice>
                <xs:element name="possibleAnswers">
                    <xs:complexType>
                        <xs:sequence maxOccurs="unbounded">
                            <xs:element name="possibleAnswer" type="possibleAnswerType"/>
                        </xs:sequence>
                    </xs:complexType>
                </xs:element>
                <xs:element name="freeTextAnswer" minOccurs="0">
                    <xs:annotation>
                        <xs:documentation>
                            @ISO: NOT USED
                        </xs:documentation>
                    </xs:annotation>
                    <xs:complexType>
                        <xs:sequence>
                            <xs:element name="obligation">
                                <xs:annotation>
                                    <xs:documentation>
                                        Possible values:
                                        @IEC: MANDATORY, OPTIONAL
                                    </xs:documentation>
                                </xs:annotation>
                            </xs:element>
                            <xs:element name="format" type="contentFormatType"/>
                            <xs:element name="contentLength"/>
                        </xs:sequence>
                    </xs:complexType>
                </xs:element>
            </xs:choice>
        </xs:sequence>
    </xs:complexType>

    <xs:complexType name="possibleAnswerType">
        <xs:sequence>
            <xs:element name="position" type="xs:decimal"/>
            <xs:element name="text"/>
            <xs:element name="value" type="xs:string" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>
                        @IEC: Technical storage. Associated to the text via lookup table
                        @ISO: Not used.
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="formal" type="answerFormalType" minOccurs="0"/>
            <xs:element name="feedback" type="feedbackType" minOccurs="0"/>
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="feedbackType">
        <xs:annotation>
            <xs:documentation>
                Description of the obligation and type of feedback expected to be provided by a voter
                when answering a specific value to a question.
                For example, when a disapproval vote is cast, is any input or justification or "feedback"
                expected to be provided along with the disapproval.
            </xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="obligation" type="obligationType">
                <xs:annotation>
                    <xs:documentation>
                        Specify the obligation level
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="media" type="feedbackTypeType" minOccurs="0">
            </xs:element>
        </xs:sequence>
    </xs:complexType>
    <xs:simpleType name="feedbackTypeType">
        <xs:annotation>
            <xs:documentation>
                Specify via which channel or media, input or justification text needs to be provided to the ballot.
                This applies to inputting (1) comments on the ballot in general, see ballot/globalFeedbackMode,
                or (2) when answering a specific value to one question, see
                ballotConfiguration/voteConfiguration/questions/question/possibleAnswers/possibleAnswer/feedback/media.

            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:token">
            <xs:enumeration value="INLINE">
                <xs:annotation>
                    <xs:documentation>
                        Feedback will be provided online from a web form, as plain or rich text uploaded by the user
                        while commmeting or voting.
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="BY_ATTACHMENT">
                <xs:annotation>
                    <xs:documentation>
                        Feedback will be provided via uploading a document, for example according to the Commenting Template.
                    </xs:documentation>
                </xs:annotation>
            </xs:enumeration>
            <xs:enumeration value="BY_OSD"/>
            <xs:enumeration value="NONE"/>
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="obligationType">
        <xs:annotation>
            <xs:documentation>
                Specifiy the obligation level of providing a feedback (input or justification) along with
                specific answer value to a question.
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:token">
            <xs:enumeration value="MANDATORY"/> <!-- Feedback must be provided -->
            <xs:enumeration value="OPTIONAL"/> <!-- Feedback may be provided but is not mandatory -->
            <xs:enumeration value="ABSTENTION"/> <!-- Use case to be further clarified -->
            <xs:enumeration value="FORBIDDEN"/> <!-- Feedback is not allowed for an answer value -->
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="answerFormalType">
        <xs:annotation>
            <xs:documentation>
                Formal type is used to compute ballot outcome
            </xs:documentation>
        </xs:annotation>
        <xs:restriction base="xs:token">
            <xs:enumeration value="POSITIVE"/>
            <xs:enumeration value="NEGATIVE"/>
            <xs:enumeration value="ABSTENTION"/>
        </xs:restriction>
    </xs:simpleType>
</xs:schema>

