# SDO - Harmonized Data Models

# Core Principles

A joint project between CEN, CENELEC, IEC and ISO.

# Introduction

The documentation is now maintained in the [project's wiki](https://bitbucket.org/sdo-hapi/data-models/wiki/Home)

# Documentation, for oXygen users
For a significant modification, please generate the documentation as described below:

1. Start oXygen
2. Tools > Generate Documentation > XML Schema Documentation
3. Field "Schema URL": select the XSD file to generate, eg. data-models/schema/xsd/projects/harmonized-projects.xsd
4. Format: PDF
5. Output file: e.g. data-models/documentation/schema/pdf/projects/harmonized-projects.pdf
6. Split output into multiple files > Split by location
7. Settings: leave defaults
8. Generate
