# Original project's documents

This directory contains the very original Request For Comments documents
that were exchanged with the User Groups during the initial phases of
design of this project, in 2017 and 2018.

The information contained in these documents has NOT been maintained and is provided
as archives only.
