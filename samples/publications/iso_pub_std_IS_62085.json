{
  "source": {
    "organization": "ISO",
    "generated": "2024-05-22T08:28:32.436",
    "schemaVersion": "1.6.0"
  },
  "publication": [
    {
      "urn": "iso:pub:std:IS:62085",
      "urnAlt": [],
      "publicationType": "STANDARD",
      "reference": "ISO 9001:2015",
      "originator": "ISO",
      "stdType": "IS",
      "stdNumber": "9001",
      "edition": "5",
      "version": 1,
      "title": [
        {
          "value": "Quality management systems — Requirements",
          "lang": "en"
        },
        {
          "value": "Systèmes de management de la qualité — Exigences",
          "lang": "fr"
        }
      ],
      "committee": {
        "urn": "iso:committee:53896",
        "urnAlt": [],
        "reference": "ISO/TC 176/SC 2"
      },
      "abstract": [
        {
          "format": "text/html",
          "content": "<p>ISO 9001:2015 specifies requirements for a quality management system when an organization:</p>\n<p>a)    needs to demonstrate its ability to consistently provide products and services that meet customer and applicable statutory and regulatory requirements, and</p>\n<p>b)    aims to enhance customer satisfaction through the effective application of the system, including processes for improvement of the system and the assurance of conformity to customer and applicable statutory and regulatory requirements.</p>\n<p>All the requirements of ISO 9001:2015 are generic and are intended to be applicable to any organization, regardless of its type or size, or the products and services it provides.</p>\n",
          "lang": "en"
        },
        {
          "format": "text/html",
          "content": "<p>L'ISO 9001:2015 spécifie les exigences relatives au système de management de la qualité lorsqu'un organisme:</p>\n<p>a)    doit démontrer son aptitude à fournir constamment des produits et des services conformes aux exigences des clients et aux exigences légales et réglementaires applicables, et</p>\n<p>b)    vise à accroître la satisfaction de ses clients par l'application efficace du système, y compris les processus pour l'amélioration du système et l'assurance de la conformité aux exigences des clients et aux exigences légales et réglementaires applicables.</p>\n<p>Toutes les exigences de l'ISO 9001:2015 sont génériques et prévues pour s'appliquer à tout organisme, quels que soient son type ou sa taille, ou les produits et services qu'il fournit.</p>\n",
          "lang": "fr"
        }
      ],
      "classifications": [
        {
          "type": "ICS",
          "value": "03.100.70"
        },
        {
          "type": "ICS",
          "value": "03.120.10"
        },
        {
          "type": "LOGO",
          "value": "ISO"
        },
        {
          "type": "SUSTAINABLE_DEVELOPMENT_GOAL",
          "value": "1"
        },
        {
          "type": "SUSTAINABLE_DEVELOPMENT_GOAL",
          "value": "12"
        },
        {
          "type": "SUSTAINABLE_DEVELOPMENT_GOAL",
          "value": "14"
        },
        {
          "type": "SUSTAINABLE_DEVELOPMENT_GOAL",
          "value": "9"
        }
      ],
      "status": "PUBLISHED",
      "publicationStage": "IS",
      "publicationDate": "2015-09-22",
      "priceInfo": {
        "priceCode": "iso:E",
        "basePrice": {
          "amount": 151,
          "currency": "CHF"
        }
      },
      "releaseItems": [
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "EPUB",
          "contentLanguage": [
            "en"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-22",
          "version": 1,
          "pages": 29,
          "contentRef": {
            "id": "b58c4ea4-44bf-488b-9a93-7cbde2607423",
            "url": "https://data.iso.org/publications/std/dms/b58c4ea4-44bf-488b-9a93-7cbde2607423",
            "contentLength": 1430134,
            "mimeType": "application/epub+zip",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication ePub (en)",
            "fileExtension": "epub",
            "checksum": "SHA-256=F74CA7B6DC1602738DAFA6756CE8576FAF04A2C265A99FD8238407CF26CB201C"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "EPUB",
          "contentLanguage": [
            "es"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-22",
          "version": 1,
          "pages": 30,
          "contentRef": {
            "id": "2664c400-a3ea-4f56-b080-7473c1fac7de",
            "url": "https://data.iso.org/publications/std/dms/2664c400-a3ea-4f56-b080-7473c1fac7de",
            "contentLength": 1824956,
            "mimeType": "application/epub+zip",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication ePub (es)",
            "fileExtension": "epub",
            "checksum": "SHA-256=21CBDBFB9BDF99307519AF9A3374B0F58B974B75CA57B48A288D2AC7F0F86481"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "EPUB",
          "contentLanguage": [
            "fr"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-24",
          "version": 2,
          "pages": 31,
          "contentRef": {
            "id": "8e44b6c7-fed4-437b-9f5d-77e1a52627e2",
            "url": "https://data.iso.org/publications/std/dms/8e44b6c7-fed4-437b-9f5d-77e1a52627e2",
            "contentLength": 1559317,
            "mimeType": "application/epub+zip",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication ePub (fr) v2",
            "fileExtension": "epub",
            "checksum": "SHA-256=FC5C5CE049B839A1A09CE478FA5647CE3E2735F1EE646014AA4281C0F471832A"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "PDF",
          "formatVariant": "CPDF",
          "contentLanguage": [
            "ar"
          ],
          "multilingual": false,
          "releaseDate": "2017-02-01",
          "version": 1,
          "pages": 31,
          "contentRef": {
            "id": "07d859ad-2fe0-41eb-8834-308e9896b90b",
            "url": "https://data.iso.org/publications/std/dms/07d859ad-2fe0-41eb-8834-308e9896b90b",
            "contentLength": 863928,
            "mimeType": "application/pdf",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication PDF (ar)",
            "fileExtension": "pdf",
            "checksum": "SHA-256=5DFB75E9C898B2CC997BACC11B12DFF8B0A5CA01F871D427F1441FACC841A3B8"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "PDF",
          "formatVariant": "CPDF",
          "contentLanguage": [
            "en"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-22",
          "version": 1,
          "pages": 29,
          "contentRef": {
            "id": "83acb4d9-f9a3-492f-96e2-2bf34829cd79",
            "url": "https://data.iso.org/publications/std/dms/83acb4d9-f9a3-492f-96e2-2bf34829cd79",
            "contentLength": 887206,
            "mimeType": "application/pdf",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication PDF (en)",
            "fileExtension": "pdf",
            "checksum": "SHA-256=47732851522725A58B5DCA3FFB7C2B539C706B0D8F9BFE10CF1DEAD9178738D2"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "PDF",
          "formatVariant": "CPDF",
          "contentLanguage": [
            "es"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-22",
          "version": 1,
          "pages": 30,
          "contentRef": {
            "id": "c6db4971-bbde-44f0-aa8e-ba43510c522d",
            "url": "https://data.iso.org/publications/std/dms/c6db4971-bbde-44f0-aa8e-ba43510c522d",
            "contentLength": 946477,
            "mimeType": "application/pdf",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication PDF (es)",
            "fileExtension": "pdf",
            "checksum": "SHA-256=EDAF9B24F7559E7E6AD638C0F9644BBFBD13369627626F90EEB67C95AD01A4F4"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "PDF",
          "formatVariant": "CPDF",
          "contentLanguage": [
            "fr"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-24",
          "version": 2,
          "pages": 31,
          "contentRef": {
            "id": "442e16dc-c007-420d-8d3a-b06575c6a4c3",
            "url": "https://data.iso.org/publications/std/dms/442e16dc-c007-420d-8d3a-b06575c6a4c3",
            "contentLength": 966835,
            "mimeType": "application/pdf",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication PDF (fr) v2",
            "fileExtension": "pdf",
            "checksum": "SHA-256=AD131EB623DC69081B754D83848AC5474BD5CB3F986101B1E96443D64C0FD836"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "PDF",
          "formatVariant": "CPDF",
          "contentLanguage": [
            "ru"
          ],
          "multilingual": false,
          "releaseDate": "2015-11-20",
          "version": 1,
          "pages": 29,
          "contentRef": {
            "id": "5b706b77-4a15-45ae-9652-193abbefd297",
            "url": "https://data.iso.org/publications/std/dms/5b706b77-4a15-45ae-9652-193abbefd297",
            "contentLength": 464119,
            "mimeType": "application/pdf",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication PDF (ru)",
            "fileExtension": "pdf",
            "checksum": "SHA-256=6689AC87BE7DA015D5FA3B4F717E582B38BB09F686DF77AE2C1EBC498EBD9110"
          }
        },
        {
          "languageNeutral": false,
          "type": "REDLINE",
          "format": "PDF",
          "contentLanguage": [
            "en"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-22",
          "version": 1,
          "pages": 29,
          "contentRef": {
            "id": "bda51e08-a488-4bca-be60-c52aee5acb65",
            "url": "https://data.iso.org/publications/std/dms/bda51e08-a488-4bca-be60-c52aee5acb65",
            "contentLength": 1414008,
            "mimeType": "application/pdf",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication Redline PDF (en)",
            "fileExtension": "pdf",
            "checksum": "SHA-256=FCB48CFE8D69274C6A2D882EDD02B36E60BDA5A5DBB97149433034F62137F138"
          }
        },
        {
          "languageNeutral": false,
          "type": "REDLINE",
          "format": "PDF",
          "contentLanguage": [
            "es"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-22",
          "version": 1,
          "pages": 30,
          "contentRef": {
            "id": "29160665-7232-4d5c-a266-9c375ffa38b0",
            "url": "https://data.iso.org/publications/std/dms/29160665-7232-4d5c-a266-9c375ffa38b0",
            "contentLength": 1542172,
            "mimeType": "application/pdf",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication Redline PDF (es)",
            "fileExtension": "pdf",
            "checksum": "SHA-256=477A4B426AE91222850476DF7E1008D26C252591E835326AF49B0919BBAFAA18"
          }
        },
        {
          "languageNeutral": false,
          "type": "REDLINE",
          "format": "PDF",
          "contentLanguage": [
            "fr"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-24",
          "version": 2,
          "pages": 31,
          "contentRef": {
            "id": "5b52b9ce-e912-42d0-8040-bb0907bde5e0",
            "url": "https://data.iso.org/publications/std/dms/5b52b9ce-e912-42d0-8040-bb0907bde5e0",
            "contentLength": 1569113,
            "mimeType": "application/pdf",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication Redline PDF (fr) v2",
            "fileExtension": "pdf",
            "checksum": "SHA-256=10770E18131C31C7CD9D8FDEF423C6A186C1C2AE6C0FC7D040AD7C0C7990AE17"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "WORD",
          "contentLanguage": [
            "ar"
          ],
          "multilingual": false,
          "releaseDate": "2017-02-01",
          "version": 1,
          "pages": 31,
          "contentRef": {
            "id": "1a69f5a8-a83c-4acd-ba5c-6e8bc451ef67",
            "url": "https://data.iso.org/publications/std/dms/1a69f5a8-a83c-4acd-ba5c-6e8bc451ef67",
            "contentLength": 786150,
            "mimeType": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication Word (ar)",
            "fileExtension": "docx",
            "checksum": "SHA-256=4AA6F31F3767CD04EF60115A0B2065E6764D518B9B3C6E4DD05F2DA2AF7834A6"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "WORD",
          "contentLanguage": [
            "en"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-22",
          "version": 1,
          "pages": 29,
          "contentRef": {
            "id": "389c5bde-35d6-4688-9f4b-b9db7dbbd2da",
            "url": "https://data.iso.org/publications/std/dms/389c5bde-35d6-4688-9f4b-b9db7dbbd2da",
            "contentLength": 2035200,
            "mimeType": "application/msword",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication Word (en)",
            "fileExtension": "doc",
            "checksum": "SHA-256=335FBF941C0B0B3383DFAB4074D45197501E3DA12C1B624B5AD6FF5B99A0266E"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "WORD",
          "contentLanguage": [
            "es"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-22",
          "version": 1,
          "pages": 30,
          "contentRef": {
            "id": "5b1cd23a-0bec-47a1-99ad-7be10216e837",
            "url": "https://data.iso.org/publications/std/dms/5b1cd23a-0bec-47a1-99ad-7be10216e837",
            "contentLength": 2177024,
            "mimeType": "application/msword",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication Word (es)",
            "fileExtension": "doc",
            "checksum": "SHA-256=4BDFF8FA44201FE408432A921C50F70AC91152DA27ED5916B0CF0C6034396EEF"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "WORD",
          "contentLanguage": [
            "fr"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-24",
          "version": 2,
          "pages": 31,
          "contentRef": {
            "id": "7280c512-e07c-4fa0-b584-5998d8b8f66c",
            "url": "https://data.iso.org/publications/std/dms/7280c512-e07c-4fa0-b584-5998d8b8f66c",
            "contentLength": 2255360,
            "mimeType": "application/msword",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication Word (fr) v2",
            "fileExtension": "doc",
            "checksum": "SHA-256=9532AB55A82291032965B1023A6B11542688B236696CBD64B33FDE23A0AA97C0"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "WORD",
          "contentLanguage": [
            "ru"
          ],
          "multilingual": false,
          "releaseDate": "2015-11-20",
          "version": 1,
          "pages": 29,
          "contentRef": {
            "id": "31f8a20f-ee61-4833-8c9a-654f56ae6b55",
            "url": "https://data.iso.org/publications/std/dms/31f8a20f-ee61-4833-8c9a-654f56ae6b55",
            "contentLength": 753664,
            "mimeType": "application/msword",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication Word (ru)",
            "fileExtension": "doc",
            "checksum": "SHA-256=E543E4EC7E49D827E65F4AE34BB50632CDCE3E599EFD5C96DE0F82FB2CC0D442"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "XML",
          "formatVariant": "ISOSTS",
          "contentLanguage": [
            "en"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-22",
          "version": 1,
          "pages": 29,
          "contentRef": {
            "id": "58968861-3515-498f-b64b-37cf58247cec",
            "url": "https://data.iso.org/publications/std/dms/58968861-3515-498f-b64b-37cf58247cec",
            "contentLength": 1750796,
            "mimeType": "application/zip",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication XML (ISO-STS) (en)",
            "fileExtension": "zip",
            "checksum": "SHA-256=0339652341A29BC4438EE49B6426A5143D434A4FC3471DE140998C3BF76A16C7"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "XML",
          "formatVariant": "ISOSTS",
          "contentLanguage": [
            "es"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-22",
          "version": 1,
          "pages": 30,
          "contentRef": {
            "id": "0f1fb4bd-1784-438f-959b-d7dfd692994f",
            "url": "https://data.iso.org/publications/std/dms/0f1fb4bd-1784-438f-959b-d7dfd692994f",
            "contentLength": 1862075,
            "mimeType": "application/zip",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication XML (ISO-STS) (es)",
            "fileExtension": "zip",
            "checksum": "SHA-256=AC40F0D6192B6A3E837F6E99C4AA67723AB6729949AE2F6104666A01CA96AD5A"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "XML",
          "formatVariant": "ISOSTS",
          "contentLanguage": [
            "fr"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-24",
          "version": 2,
          "pages": 31,
          "contentRef": {
            "id": "5bb1dc2c-b46d-4e94-8baa-077272fa52da",
            "url": "https://data.iso.org/publications/std/dms/5bb1dc2c-b46d-4e94-8baa-077272fa52da",
            "contentLength": 1953947,
            "mimeType": "application/zip",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication XML (ISO-STS) (fr) v2",
            "fileExtension": "zip",
            "checksum": "SHA-256=730BC6EBFE7A51F19BDF2EBECC23411845D1BE4852D99DA0D2977743CC19FD46"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "XML",
          "formatVariant": "ISOSTS",
          "contentLanguage": [
            "ru"
          ],
          "multilingual": false,
          "releaseDate": "2015-11-20",
          "version": 1,
          "pages": 29,
          "contentRef": {
            "id": "a5ba5479-59ba-4aa2-9542-43ba392d9f35",
            "url": "https://data.iso.org/publications/std/dms/a5ba5479-59ba-4aa2-9542-43ba392d9f35",
            "contentLength": 408887,
            "mimeType": "application/zip",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication XML (ISO-STS) (ru)",
            "fileExtension": "zip",
            "checksum": "SHA-256=AC33D58127D852E824BB9D0404B18F8B5F0BE78D825F81AAD04B3842BA8D7BB1"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "XML",
          "formatVariant": "NISOSTS",
          "contentLanguage": [
            "en"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-22",
          "version": 1,
          "pages": 29,
          "contentRef": {
            "id": "2b9fb47f-ec92-4b54-b2f4-7e948d6514b5",
            "url": "https://data.iso.org/publications/std/dms/2b9fb47f-ec92-4b54-b2f4-7e948d6514b5",
            "contentLength": 1751194,
            "mimeType": "application/zip",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication XML (NISO-STS) (en)",
            "fileExtension": "zip",
            "checksum": "SHA-256=EB648A7BBA38E3FB1AF50ABA9DCEF5DC870AC014FE636010C14CE1C8B7A1B87D"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "XML",
          "formatVariant": "NISOSTS",
          "contentLanguage": [
            "es"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-22",
          "version": 1,
          "pages": 30,
          "contentRef": {
            "id": "c259b8b9-0166-41b4-8fa4-873e00a30387",
            "url": "https://data.iso.org/publications/std/dms/c259b8b9-0166-41b4-8fa4-873e00a30387",
            "contentLength": 1862540,
            "mimeType": "application/zip",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication XML (NISO-STS) (es)",
            "fileExtension": "zip",
            "checksum": "SHA-256=99841142743D91CC1D9F74C1B909CE08C65008D847FC3F889C6158F80902B99E"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "XML",
          "formatVariant": "NISOSTS",
          "contentLanguage": [
            "fr"
          ],
          "multilingual": false,
          "releaseDate": "2015-09-24",
          "version": 2,
          "pages": 31,
          "contentRef": {
            "id": "026da105-f5cb-4bbd-a2e5-5e32461388dc",
            "url": "https://data.iso.org/publications/std/dms/026da105-f5cb-4bbd-a2e5-5e32461388dc",
            "contentLength": 1954437,
            "mimeType": "application/zip",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication XML (NISO-STS) (fr) v2",
            "fileExtension": "zip",
            "checksum": "SHA-256=D77D502EA646C56EF0B7811E01B74F1FDC4E894CEB2BE83BB3E335624CE41F7C"
          }
        },
        {
          "languageNeutral": false,
          "type": "STANDARD",
          "format": "XML",
          "formatVariant": "NISOSTS",
          "contentLanguage": [
            "ru"
          ],
          "multilingual": false,
          "releaseDate": "2015-11-20",
          "version": 1,
          "pages": 29,
          "contentRef": {
            "id": "2cd5e405-3113-49df-803f-fed875852e91",
            "url": "https://data.iso.org/publications/std/dms/2cd5e405-3113-49df-803f-fed875852e91",
            "contentLength": 409110,
            "mimeType": "application/zip",
            "fileName": "ISO 9001;2015 ed.5 - id.62085 Publication XML (NISO-STS) (ru)",
            "fileExtension": "zip",
            "checksum": "SHA-256=BBBDACB94D5DEEB44A9670D9383B015EDCD9AE8DBA677955DC4551CF25D76789"
          }
        }
      ],
      "project": {
        "urn": "iso:proj:62085",
        "urnAlt": []
      },
      "lastChangeTimestamp": "2024-01-03T09:25:08.967"
    }
  ]
}