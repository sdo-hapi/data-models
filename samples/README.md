# Sample response bodies

The files contained under this directory contain samples of XML and JSON response bodies
when you invoke the APIs.

You can use the following sample code to extract or reproduce them (or others).

```
accesstoken=XXXXXXXXXXXXXXXXXX  (access token as obtained from the Authorization Server)

curl -q --header "Authorization: Bearer $accesstoken" https://api-test.iso.org/harmonized/projects/iso:proj:62085 | jq . >projects/iso_proj_62085.json
curl -q --header "Authorization: Bearer $accesstoken" --header "Accept: application/xml" https://api-test.iso.org/harmonized/projects/iso:proj:62085 | xmllint --format - >projects/iso_proj_62085.xml

curl -q --header "Authorization: Bearer $accesstoken" https://api-test.iso.org/harmonized/publications/iso:pub:std:CD:62085 | jq . >publications/iso_pub_std_CD_62085.json
curl -q --header "Authorization: Bearer $accesstoken" --header "Accept: application/xml" https://api-test.iso.org/harmonized/publications/iso:pub:std:CD:62085 | xmllint --format - >publications/iso_pub_std_CD_62085.xml
curl -q --header "Authorization: Bearer $accesstoken" https://api-test.iso.org/harmonized/publications/iso:pub:std:DIS:62085 | jq . >publications/iso_pub_std_DIS_62085.json
curl -q --header "Authorization: Bearer $accesstoken" --header "Accept: application/xml" https://api-test.iso.org/harmonized/publications/iso:pub:std:DIS:62085 | xmllint --format - >publications/iso_pub_std_DIS_62085.xml
curl -q --header "Authorization: Bearer $accesstoken" https://api-test.iso.org/harmonized/publications/iso:pub:std:FDIS:62085 | jq . >publications/iso_pub_std_FDIS_62085.json
curl -q --header "Authorization: Bearer $accesstoken" --header "Accept: application/xml" https://api-test.iso.org/harmonized/publications/iso:pub:std:FDIS:62085 | xmllint --format - >publications/iso_pub_std_FDIS_62085.xml
curl -q --header "Authorization: Bearer $accesstoken" https://api-test.iso.org/harmonized/publications/iso:pub:std:IS:62085 | jq . >publications/iso_pub_std_IS_62085.json
curl -q --header "Authorization: Bearer $accesstoken" --header "Accept: application/xml" https://api-test.iso.org/harmonized/publications/iso:pub:std:IS:62085 | xmllint --format - >publications/iso_pub_std_IS_62085.xml


```
