Harmonized Interfaces - Data Models
===================================

Harmonized data models for metadata of projects (i.e. standards) and publications.
A joint project between CEN, CENELEC, IEC and ISO.

## Sources & References

This document is part of the GIT repository at: https://bitbucket.org/sdo-hapi/data-models/

ISO users, please submit issues on ISO's Jira at: https://support.iso.org/issues/projects/ISOBB/summary  (use the component "Harmonized interfaces")


## Documentation

### For users

If you need a PDF documentation of our schema, you may find those in subdirectory "documentation/schema/pdf",
or generate with you preferred XML development environment.

### For maintainers

Our schema are documented with XSD's "annotation" and "documentation" elements. To favor the separation of concerns of
functional modifications to our schema vs. non-functional improvements to the documentation, we have decided to regroup all
documentation in an XML catalog, available in this repository in subdirectory "schema/xsd/_catalog".

There you will find 5 files:
- documentation.xml - contains the common definitions of all data elements
- cen-specific.xml, cenelec-specific.xml, iec-specific.xml, iso-specific.xml - documents particular usage of the data element for the specified SDO

Each file defines entries with a "key" and a "description". The key is referenced from the XSD "appinfo" value in our schema.
After any update to the catalog's definitions, you have to inject the new values in the XSD schemas. For this you use XSLT.

Generate XSDs with updated documentation
```
for f in `find schema/xsd -name \*.xsd`; do 
  echo $f
  xsltproc -o "$f-transformed.xsd" schema/xsd/_catalog/remap.xsl "$f"
done
```

Then compare and possibly commit the new XSDs.
```
for f in `find schema/xsd -name \*-transformed.xsd`; do 
  echo $f
  mv  "$f" `dirname $f`/`basename $f -transformed.xsd`
done
```
